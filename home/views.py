import string
import random
import os

from django.shortcuts import render, redirect

# Create your views here.
from home.models import Data


def home(request):
    items = Data.objects.all()
    return render(request, "home.html", {"items": items, "host": os.uname()[1]})

def add(request):
    random_text = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
    data = Data(data=random_text)
    data.save()
    return redirect("/")
