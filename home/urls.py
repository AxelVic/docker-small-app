from django.urls import path

from home.views import home, add

app_name = 'home'
urlpatterns = [
    path("", home, name="home"),
    path("add/", add, name="add")
]