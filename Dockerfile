FROM python:3
ENV  PYTHONDONTWRITEBYTECODE 1
ENV DB_NAME smallappdata
ENV DB_USER root
ENV DB_PASSWORD worker
ENV DB_HOST db
ENV DB_PORT 3306

# File Author / Maintainer
MAINTAINER Axel

#add project files to the usr/src/app folder
ADD . /usr/src/app

#set directoty where CMD will execute
WORKDIR /usr/src/app

COPY requirements.txt ./

# Get pip to download and install requirements:
RUN pip install --no-cache-dir -r requirements.txt

# Expose ports
EXPOSE 8000

# default command to execute
CMD exec gunicorn smallapp.wsgi:application --bind 0.0.0.0:8000 --workers 3